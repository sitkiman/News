<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'theme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i[hRN?A{78L}3k|o:O5Qh^<#y8;%1k$|E3<Wzs3+S Xev74H$v$DCa.8`xp|`v!S');
define('SECURE_AUTH_KEY',  '&=@jJlNo)w!RZ.cnR^*nEroMM2gD(kM8E0GDA[BeNF4`o*[E1OJvq5i=i%fC_6dO');
define('LOGGED_IN_KEY',    '>9J<C%0gvY<(b_8ZxgBVJ){3tB&*q<UAd4?lha7Z(X3ohW]dq5*gYO/eiFga~[Yu');
define('NONCE_KEY',        'c8O@[4%Yg07oT0Xsx$Z.Ki6$AWwKhSV-[Uh[km$+~?31.h5VR6bm+D=bQG}m4>92');
define('AUTH_SALT',        '<+we]$u5}CcOYVgD2RQA,m&Ra,ccMj%;_KoE1=b jrS8=y+dxy ew7K[MblW+,q6');
define('SECURE_AUTH_SALT', '0aic0|*5v>c4ZZ&GR5IWeNnz;2oC_HHb*k.l(Ow/#!N5A1,?P|@Xe}2K(ROp5w9-');
define('LOGGED_IN_SALT',   'H(^:B$`zvlMe;83>@C@{S4CE0~0:cy%G@5ZO!-iSpDrk1Y,jU$4.GH FvR#v]I{p');
define('NONCE_SALT',       'MA?89sW/E^HhAvAqi6{[rVF+{lTG;Q9u`1-.i>If6oO_=aZ.i}e-RO OVk=J>S_c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
